addons = require '../addons/'
EventEmitter = require("events").EventEmitter

class ThoughtProcessor extends EventEmitter
	
	constructor: ->
		self = this
		addons.on 'notification', (message) ->
			self.process message
	
	process: (message, from) ->
		@emit 'thought', message, from

module.exports = new ThoughtProcessor
