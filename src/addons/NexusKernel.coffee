spawn			= require('child_process').spawn
cronJob			= require('cron').CronJob
EventEmitter	= require("events").EventEmitter

class NexusKernel extends EventEmitter
	
	init: ->
		cronTab =
			cronTime: '00 30 11 * * 0-6' # runs every day at 11:30 AM
			onTick: this.run(this)
			start: false
		
		job = new cronJob cronTab
		job.start()

	run: (self) ->
		self.emit 'notification', 'Spawning build for Nexus 4 Kernel'
		gen_kernel = spawn "#{process.env.HOME}/android/gen_kernel"
		gen_kernel.stdout.on 'data', (data) ->
			console.log data.toString()

		gen_kernel.stderr.on 'data', (data) ->
			console.log data.toString()

		gen_kernel.on 'exit', (code) ->
			if code == 0
				self.emit 'notification', 'Build succeeded'
			else
				self.emit 'notification', "Build exited with code #{code}"

module.exports = new NexusKernel