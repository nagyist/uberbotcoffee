EventEmitter = require("events").EventEmitter

class AddonIndex extends EventEmitter
	
	constructor: ->
		# list of addons
		addons = ['NexusKernel']
		this.importAddons addon for addon in addons
		
	importAddons: (addon) ->
		self = this
		addonPtr = require "./#{addon}"
		addonPtr.init()
		addonPtr.on 'notification', (message, to) ->
			self.emit 'notification', message, to

module.exports = new AddonIndex