cheerio = require 'cheerio'
request = require 'request'
EventEmitter = require("events").EventEmitter

class BitcoinGem extends EventEmitter
	
	url:	'http://bitcoingem.com/'
	price:	0
	
	init: ->
		self = this
		setInterval this.run, 10000, self
	
	run: (self) ->
		self.getCurrentPrice (price) ->
			if price == self.price
				# don't need to do anything
			else if price > self.price
				self.emit 'notification', "Bitcoin Gem price rose to #{price} from #{self.price}"
				self.price = price
			else if price < self.price
				self.emit 'notification', "Bitcoin Gem price dropped to #{price} from #{self.price}"
				self.price = price

	getCurrentPrice: (cb) ->
		request this.url, (err, res, body) ->
			$ = cheerio.load body
			price = $('h2').toString().match(/(\d+\.\d+)/)[0]
			cb parseFloat(price)
		
module.exports = new BitcoinGem

