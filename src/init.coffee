xmpp	= require 'simple-xmpp'
spawn	= require('child_process').spawn

config =
	jid:		process.argv[2]
	password:	process.argv[3]
	host:		'talk.google.com'
	post:		5222
	master:		'ithinkincode@gmail.com'

xmpp.on 'online', ->
	console.log 'Connected'

xmpp.on 'chat', (from, message) ->
	brain.process message, from

xmpp.on 'buddy', (jid, state) ->
	#console.log "#{jid} state is now #{state}"

xmpp.on 'error', (error) ->
	console.error error

xmpp.connect config

brain = require './lib/ThoughtProcessor'

# UberBot always speaks his mind
brain.on 'thought', (message, from) ->
	if from?
		if from == config['master']
			spawn 'espeak', ["#{message}"]
		
		xmpp.send from, message
	else
		spawn 'espeak', ["#{message}"]
		xmpp.send config['master'], message